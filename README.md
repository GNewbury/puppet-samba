# samba

Welcome to your new module. A short overview of the generated parts can be found in the PDK documentation at https://puppet.com/docs/pdk/latest/pdk_generating_modules.html .

The README template below provides a starting point with details about what information to include in your README.

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with samba](#setup)
    * [What samba affects](#what-samba-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with samba](#beginning-with-samba)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Limitations - OS compatibility, etc.](#limitations)
5. [Development - Guide for contributing to the module](#development)

## Description

This is a simple no-frills module for setting up a Samba share. It currently consumes a map of all config you'd find in smb.conf, and an array of the services the samba package will install.

## Setup

There are currently no dependencies, as this module uses only native Puppet datatypes.

### Beginning with samba

```
include samba
```

## Usage

The samba class can either be passed variables, or can have hiera values set.

### Variable passing
```
$_config = {
  'global' => {
    'k' => 'v',
  },
  'shares' => {
    'my-share-1' => {
      'k' => 'v',
    },
    'my-share-2' => {
      'k' => 'v
    },
  }
  'printers' => false,
}

class samba {
  configs => $_config
}
```

### Hiera
```yaml
samba::configs:
  global:
    k: 'v'
  shares:
    my-share-1:
      k: 'v'
    my-share-2:
      k: 'v'
  printers: false
```

```
include samba
```

## Limitations

In the Limitations section, list any incompatibilities, known issues, or other warnings.

## Development

In the Development section, tell other users the ground rules for contributing to your project and how they should submit their work.

## Release Notes/Contributors/Etc. **Optional**

If you aren't using changelog, put your release notes here (though you should consider using changelog). You can also add any additional sections you feel are necessary or important to include here. Please use the `## ` header.
