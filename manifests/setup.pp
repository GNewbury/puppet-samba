# Samba setup

class samba::setup {

  package {'samba':
    ensure => present
  }

}
