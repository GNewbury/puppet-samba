# Samba services

class samba::service {

  service { $::samba::services:
    ensure => running,
    enable => true,
  }

}
