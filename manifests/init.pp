# Main samba class
#
# @param configs  - Map of your smb.conf file. Contains nested maps under $configs['global'], etc
# @param services - List of all services needed for samba
#

class samba (
  Hash   $configs    = {},
  Array  $services   = ['smbd', 'nmbd'],
  String $config_dir = '/etc/samba',
)
{

  contain samba::setup
  contain samba::config
  contain samba::service

  Class['samba::setup']
  -> Class['samba::config']
  ~> Class['samba::service']

}
