# Samba config

class samba::config {

  $_global   = $::samba::configs['global']



  if has_key($::samba::configs, 'shares') {
    $_shares = $::samba::configs['shares']
  } else {
    notice('No shares found')
    $_shares   = {}
  }


  if has_key($::samba::configs, 'printers'){
    $_printers = $::samba::configs['printers']
  } else {
    $_printers = false
  }

  file { "${::samba::config_dir}/smb.conf":
    ensure  => present,
    content => template('samba/smb.conf.erb'),
    owner   => 'root',
    group   => 'root',
  }

}
