require "spec_helper"

describe "samba" do
  let(:title) { "samba" }
  let(:node) { "test.guynewbury.co.uk" }
  let(:params) do
    {
      "configs" => {
        "global" => {
          "test" => "test",
        },
      },
    }
  end

  it { is_expected.to compile }
end
